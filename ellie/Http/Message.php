<?php

namespace Ellie\Http;


use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\StreamInterface;

class Message implements MessageInterface
{
    public function getProtocolVersion()
    {

    }

    public function withProtocolVersion($version)
    {

    }

    public function getHeaders(): array
    {

    }

    public function hasHeader($name): bool
    {

    }


    public function getHeader($name): array
    {

    }

    public function getHeaderLine($name)
    {

    }

    public function withHeader($name, $value)
    {

    }

    public function withAddedHeader($name, $value)
    {

    }

    public function withoutHeader($name)
    {

    }

    public function getBody(): StreamInterface
    {

    }

    public function withBody(StreamInterface $body)
    {

    }

}