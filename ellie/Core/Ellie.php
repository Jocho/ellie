<?php

namespace Ellie\Core;


use Ellie\Http\Stream;
use Ellie\Http\Request;
use Ellie\Http\Response;

class Ellie
{
    /** @var Stream */
    public $stream;

    /** @var Request */
    public $request;

    /** @var Response */
    public $response;

    /** @var Router */
    public $router;

    /** @var Settings */
    public $settings;

    public function __construct()
    {
        $this->request = new Request();
        $this->response = new Response();
        $this->stream = new Stream();
        $this->router = new Router();
        $this->settings = new Settings();
    }

    public function run()
    {
        $match = $this->settings->matchRoute($this->request->getMethod(), $this->request->getUri());

        var_dump ($match);
    }
}