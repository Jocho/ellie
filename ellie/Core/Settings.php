<?php

namespace Ellie\Core;

class Settings
{
    /** @var string */
    private $environment;

    /** @var array */
    private $settings = [];

    /** @var array */
    private $routes = [];

    public function __construct()
    {

    }

    /**
     * @param string $env
     */
    public function setEnvironment(string $env)
    {
        $this->environment = $env;
    }

    /**
     * @return string
     */
    public function getEnvironment(): ?string
    {
        return $this->environment;
    }

    /**
     * @param string $path
     */
    public function setRoutes(string $path)
    {
        $this->routes = [];

        $h = fopen($path, 'r');
        echo "<pre>";

        while ($line = fgets($h)) {
            if (preg_match('/^\s$/', $line))
                continue;

            preg_match('/(?<offset>\s*)(?<request>GET|POST|OPTION|DELETE|PUT|HEAD|CONNECT|TRACE|GROUP)\s+(?<path>[a-zA-Z0-9\/\.\-_~!$&\'()*+,;=:@{}]+)\s+((?<middleware>[\w, ]+)(\||\s*$))?((?<controller>.\w+)\@(?<method>\w+))?/i', $line, $matches);

            $line = preg_replace('/\s{2,}/', ' ', trim($line));

            print_r("> line:       '" . $line . "'\n");
            print_r("> offset:     " . strlen($matches['offset']) . "\n");
            print_r("> request:    " . $matches['request'] . "\n");
            print_r("> path:       " . $matches['path'] . "\n");
            print_r("> middleware: " . (strlen($matches['middleware']) ? $matches['middleware'] : "<none>") . "\n");
            print_r("> controller: " . $matches['controller'] . "\n");
            print_r("> method:     " . $matches['method'] . "\n");

            echo "\n";
        }

        fclose($h);
        echo "</pre>";

        $offset = 0;
    }

    /**
     * @param string $path
     */
    public function setSettings(string $path)
    {
        $this->settings = include $path;
    }

    public function getSettings(): array
    {
        return $this->settings;
    }

    /**
     * @param string $requestMethod
     * @param string $path
     *
     * @return array
     */
    public function matchRoute(string $requestMethod, string $path)
    {
        return [
            'middleware' => [],
            'controller' => '',
            'method'     => ''
        ];
    }
}