<?php

use Ellie\Core\Ellie;

define('ENVIRONMENT', 'development');

try {
    $app = new Ellie();

    $app->settings->setEnvironment(ENVIRONMENT);
    $app->settings->setSettings('./app/settings/' . ENVIRONMENT . '.php');
    $app->settings->setRoutes('./app/settings/routes.txt');

    $app->run();

    echo file_get_contents('./app/view/homepage.html');
}
catch (Exception $e) {
    print_r($e->getMessage());
    die();
}